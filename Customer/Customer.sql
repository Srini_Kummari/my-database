CREATE TABLE Customer(
	CustomerId int not null identity(1,1) constraint pk_Customer_CID PRIMARY KEY CLUSTERED,
	CustomerName varchar(100),
	DoB date,
	Address varchar(500),
	city varchar(100),
	Country varchar(100),
	region varchar(100)
)

