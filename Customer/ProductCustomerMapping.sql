CREATE TABLE ProductCustomerMapping(
PCId int not null identity(1,1) constraint pk_ProdCustMap_Id PRIMARY KEY CLUSTERED,
ProductId int null constraint fk_ProdCust_Prod_PId FOREIGN KEY REFERENCES Product(ProductId),
ProductId int null constraint fk_ProdCust_Cust_PId FOREIGN KEY REFERENCES Customer(CustomerId),
InsertDate datetime,
ModifiedDate datetime
)
