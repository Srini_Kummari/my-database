CREATE TABLE ProductMaster(
ProductId int not null identity(1,1) constraint pk_ProductMaster_PID PRIMARY KEY CLUSTERED,
ProductName varchar(50) not null constraint uk_ProductMaster_PN UNIQUE,
ProductDesc varchar(200),
CreatedDate datetime,
ModifiedDate datetime
)

