CREATE TABLE ProductPrice(
	PPId int not null identity(1,1) constraint PK_PP_PPId PRIMARY KEY CLUSTERED,
	ProductId int not null constraint FK_PP_PId REFERENCES Product(ProductId),
	Price money,
	CreatedDate datetime,
	ModifiedDate datetime
)
